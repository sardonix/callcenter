package com.callcenter;

public enum CallStatus {

	ONLINE, ONHOLD, ENDED
}
