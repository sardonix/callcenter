package com.callcenter;

public class Configuration {

	private int operators;
	private int supervisors;
	private int directors;
	
	public int getOperators() {
		return operators;
	}
	public void setOperators(int operators) {
		this.operators = operators;
	}
	public int getSupervisors() {
		return supervisors;
	}
	public void setSupervisors(int supervisors) {
		this.supervisors = supervisors;
	}
	public int getDirectors() {
		return directors;
	}
	public void setDirectors(int directors) {
		this.directors = directors;
	}
	
}
