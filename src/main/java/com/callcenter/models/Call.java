package com.callcenter.models;

import com.callcenter.CallStatus;

public class Call {

	private Integer callId;
	private CallStatus status;
	private Employee employee;

	public Call(Integer callId) {
		this.callId = callId;
	}

	public CallStatus getStatus() {
		return status;
	}

	public void setStatus(CallStatus status) {
		this.status = status;
	}
	
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return callId.toString();
	}
}
