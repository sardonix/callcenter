package com.callcenter.models;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class EmployeePool {

	private EmployeePool nextCallProcessor;
	
	private Queue<Employee> queue = new ConcurrentLinkedQueue<Employee>();
	
	public void setNext(EmployeePool dispense) {
		this.nextCallProcessor = dispense;
	}
	
	public Employee getAvailableEmployee(){
		Employee availableEmployee = queue.poll();
		
		if( availableEmployee == null && nextCallProcessor != null ){
			System.out.println(" no tengo employee voy al que viene ");
			return nextCallProcessor.getAvailableEmployee();
		}
		
		return availableEmployee;
	}
	
	public void restoreEmployee(Employee employee){
		queue.add(employee);
	}
}
