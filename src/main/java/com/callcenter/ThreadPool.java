package com.callcenter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {

	public static final int MAX_ONGOING_CALLS = 10;

	private static ThreadPool instance;
	private ExecutorService fixedPool;
	private int maxConcurrentThreads = 10;

	public static ThreadPool getInstance() {
		if (instance == null) {
			instance = new ThreadPool();
		}
		return instance;
	}

	private ThreadPool() {
		fixedPool = Executors.newFixedThreadPool(maxConcurrentThreads);
	}

	public ExecutorService getFixedPool() {
		return fixedPool;
	}

}
