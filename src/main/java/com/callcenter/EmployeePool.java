package com.callcenter;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.callcenter.models.Employee;
/**
 * Employee pool works as a chain of command.
 * Being nextEmployeePoll the next chain in the responsibility line.
 */
public class EmployeePool {

	private EmployeePool nextEmployeePoll;

	private Queue<Employee> queue = new ConcurrentLinkedQueue<Employee>();

	public void setNext(EmployeePool dispense) {
		this.nextEmployeePoll = dispense;
	}

	public Employee getAvailableEmployee() {
		Employee availableEmployee = queue.poll();

		if (availableEmployee == null && nextEmployeePoll != null) {
			return nextEmployeePoll.getAvailableEmployee();
		}

		return availableEmployee;
	}

	/**
	 * when a employee get free, it is restore to the pool to be called again 
	 */
	public void restoreEmployee(Employee employee) {
		queue.add(employee);
	}
}
