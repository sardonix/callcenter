package com.callcenter;

import java.util.Random;

import com.callcenter.models.Call;

public class CallProcess implements Runnable {

	private Call call;
	private Random random = new Random();
	private Dispatcher dispatcher;

	public void setCall(Call call) {
		this.call = call;
	}

	public void setDispatcher(Dispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}

	@Override
	public void run() {
		// do what you must
		System.out.println("call " + call.toString() + " starts");

		call.setStatus(CallStatus.ONLINE);

		int result = random.nextInt(15 - 5) + 5;

		System.out.println("wait " + result * 1000);

		try {
			Thread.sleep(result * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// set call to ended
		call.setStatus(CallStatus.ENDED);

		// restore employee to be available again
		dispatcher.restoreEmployee(call.getEmployee());

		// notify call has finished.
		dispatcher.callFinished();

	}

}
