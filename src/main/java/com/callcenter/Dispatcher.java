package com.callcenter;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.callcenter.models.Call;
import com.callcenter.models.Director;
import com.callcenter.models.Employee;
import com.callcenter.models.Operator;
import com.callcenter.models.Supervisor;

public class Dispatcher {

	private Queue<Call> onholdCallsQueue = new ConcurrentLinkedQueue<Call>();
	private EmployeePool operatorsPool;
	private EmployeePool supervisorsPool;
	private EmployeePool directorsPool;

	public Dispatcher(Configuration configuration) {

		operatorsPool = new EmployeePool();
		supervisorsPool = new EmployeePool();
		directorsPool = new EmployeePool();

		operatorsPool.setNext(supervisorsPool);
		supervisorsPool.setNext(directorsPool);

		// set employees.
		for (int i = 0; i < configuration.getOperators(); i++) {
			operatorsPool.restoreEmployee(new Operator());
		}

		for (int i = 0; i < configuration.getSupervisors(); i++) {
			supervisorsPool.restoreEmployee(new Supervisor());
		}

		for (int i = 0; i < configuration.getDirectors(); i++) {
			directorsPool.restoreEmployee(new Director());
		}
	}

	/**
	 * Finds employee and dispatch the call.
	 * If no employee was found, it returns false;
	 */
	public synchronized boolean dispatchCall(Call call) {

		// get available employee
		Employee employee = operatorsPool.getAvailableEmployee();

		if (employee == null) {
			System.out.println("no officer availble for call " + call.toString() + " put on hold.");
			call.setStatus(CallStatus.ONHOLD);
			onholdCallsQueue.add(call);
			return false;
		}

		call.setEmployee(employee);
		
		CallProcess callProcess = new CallProcess();
		callProcess.setCall(call);
		callProcess.setDispatcher(this);

		ThreadPool.getInstance().getFixedPool().execute(callProcess);

		return true;
	}

	/**
	 * when a called is finished it looks for onhold calls to put online.
	 */
	public synchronized void callFinished() {
		if (onholdCallsQueue.isEmpty()) {
			return;
		}

		Call call = onholdCallsQueue.peek();
		System.out.println("resuming call " + call.toString());

		boolean status = dispatchCall(call);

		if (status) {
			onholdCallsQueue.poll();
			System.out.println("resuming call " + call.toString() + " has been successful ");
		} else {
			System.out.println("resuming call " + call.toString() + " has failed ");
		}
	}

	public void restoreEmployee(Employee employee) {

		if (employee instanceof Operator) {
			operatorsPool.restoreEmployee(employee);
		} else if (employee instanceof Supervisor) {
			supervisorsPool.restoreEmployee(employee);
		} else {
			directorsPool.restoreEmployee(employee);
		}
	}

}
