package com.test.dispatcher;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.callcenter.CallStatus;
import com.callcenter.Configuration;
import com.callcenter.Dispatcher;
import com.callcenter.models.Call;
import com.callcenter.models.Director;
import com.callcenter.models.Operator;
import com.callcenter.models.Supervisor;

public class CallsTest {
	
	/**
	 * test 10 calls, with only operators
	 */
	@Test
	public void test10CallsOnlyOperators() {
		Configuration configuration = new Configuration();
		configuration.setOperators(10);
		configuration.setSupervisors(0);
		configuration.setDirectors(0);
		
		Dispatcher dispatcher = new Dispatcher(configuration);

		List<Call> calls = new ArrayList<Call>();

		for (int i = 0; i < 10; i++) {
			Call call = new Call(i);
			dispatcher.dispatchCall(call);
			calls.add(call);
		}

		boolean allFinished = false;

		while (!allFinished) {
			allFinished = true;
			for (Call call : calls) {
				if (!CallStatus.ENDED.equals(call.getStatus())) {
					allFinished = false;
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		System.out.println("all calls finished");
		Assert.assertTrue(allFinished);
		
		for( Call call : calls){
			Assert.assertTrue(call.getEmployee() instanceof Operator);
		}
	}
	
	/**
	 * test 10 calls, with only supervisors
	 */
	@Test
	public void test10CallsOnlySupervisors(){
		Configuration configuration = new Configuration();
		configuration.setOperators(0);
		configuration.setSupervisors(10);
		configuration.setDirectors(0);
		
		Dispatcher dispatcher = new Dispatcher(configuration);

		List<Call> calls = new ArrayList<Call>();

		for (int i = 0; i < 10; i++) {
			Call call = new Call(i);
			dispatcher.dispatchCall(call);
			calls.add(call);
		}

		boolean allFinished = false;

		while (!allFinished) {
			allFinished = true;
			for (Call call : calls) {
				if (!CallStatus.ENDED.equals(call.getStatus())) {
					allFinished = false;
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		System.out.println("all calls finished");
		Assert.assertTrue(allFinished);
		
		for( Call call : calls){
			Assert.assertTrue(call.getEmployee() instanceof Supervisor);
		}
	}
	
	/**
	 * test 10 calls, with only directors
	 */
	@Test
	public void test10CallsOnlyDirectors() {
		Configuration configuration = new Configuration();
		configuration.setOperators(0);
		configuration.setSupervisors(0);
		configuration.setDirectors(10);
		
		Dispatcher dispatcher = new Dispatcher(configuration);

		List<Call> calls = new ArrayList<Call>();

		for (int i = 0; i < 10; i++) {
			Call call = new Call(i);
			dispatcher.dispatchCall(call);
			calls.add(call);
		}

		boolean allFinished = false;

		while (!allFinished) {
			allFinished = true;
			for (Call call : calls) {
				if (!CallStatus.ENDED.equals(call.getStatus())) {
					allFinished = false;
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		System.out.println("all calls finished");
		Assert.assertTrue(allFinished);
		
		for( Call call : calls){
			Assert.assertTrue(call.getEmployee() instanceof Director);
		}
	}
	
	/**
	 * test 10 calls, with only 1 director
	 * 
	 * The director should take all calls.
	 */
	@Test
	public void test10CallsOnlyOneDirector() {
		Configuration configuration = new Configuration();
		configuration.setOperators(0);
		configuration.setSupervisors(0);
		configuration.setDirectors(1);
		
		Dispatcher dispatcher = new Dispatcher(configuration);

		List<Call> calls = new ArrayList<Call>();

		for (int i = 0; i < 10; i++) {
			Call call = new Call(i);
			dispatcher.dispatchCall(call);
			calls.add(call);
		}

		boolean allFinished = false;

		while (!allFinished) {
			allFinished = true;
			for (Call call : calls) {
				if (!CallStatus.ENDED.equals(call.getStatus())) {
					allFinished = false;
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		System.out.println("all calls finished");
		Assert.assertTrue(allFinished);
		
		for( Call call : calls){
			Assert.assertTrue(call.getEmployee() instanceof Director);
		}
	}
	
	/**
	 * test 20 calls, with all employee on work
	 */
	@Test
	public void test20CallsFullEmployes() {
		Configuration configuration = new Configuration();
		configuration.setOperators(6);
		configuration.setSupervisors(3);
		configuration.setDirectors(1);
		
		Dispatcher dispatcher = new Dispatcher(configuration);

		List<Call> calls = new ArrayList<Call>();

		for (int i = 0; i < 20; i++) {
			Call call = new Call(i);
			dispatcher.dispatchCall(call);
			calls.add(call);
		}

		boolean allFinished = false;

		while (!allFinished) {
			allFinished = true;
			for (Call call : calls) {
				if (!CallStatus.ENDED.equals(call.getStatus())) {
					allFinished = false;
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		System.out.println("all calls finished");
		Assert.assertTrue(allFinished);
	}
	
	/**
	 * test 20 calls, with all shortage of personal
	 */
	@Test
	public void test20CallWithShortageOfPersonal() {
		Configuration configuration = new Configuration();
		configuration.setOperators(2);
		configuration.setSupervisors(2);
		configuration.setDirectors(2);
		
		Dispatcher dispatcher = new Dispatcher(configuration);

		List<Call> calls = new ArrayList<Call>();

		for (int i = 0; i < 20; i++) {
			Call call = new Call(i);
			dispatcher.dispatchCall(call);
			calls.add(call);
		}

		boolean allFinished = false;

		while (!allFinished) {
			allFinished = true;
			for (Call call : calls) {
				if (!CallStatus.ENDED.equals(call.getStatus())) {
					allFinished = false;
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		System.out.println("all calls finished");
		Assert.assertTrue(allFinished);
	}
}
